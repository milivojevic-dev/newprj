<?php
session_start();
require("configDatabase.php");
?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>newProject</title>
        <meta charset="UTF-8">
    </head>
    <body>
    <?php
    if(isset($_SESSION["id_user"])){
        ?>
        <form action="addProject.php" method="post" enctype="multipart/form-data">
            Category:
            <select name="category">
                <?php
                $sql = "SELECT * FROM category";
                $result = mysqli_query($connection, $sql);

                if(mysqli_num_rows($result) > 0)
                {
                    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
                    {
                        echo "<option value='".$row["id_category"]."'>".$row["name"]."</option>";
                    }
                }
                ?>
            </select>
            File:
            <input type="file" name="image" accept="image/jpeg">
            <br><br>
            Status:
            <select name="status">
                <option value="1">Defined</option>
                <option value="2">In progress</option>
                <option value="3">Finished</option>
                <option value="4">Rejected</option>
            </select>
            <br><br>
            Price:
            <input type="text" name="price">
            <br><br>
            Date start:
            <input type="date" name="datestart">
            <br><br>
            Date finish:
            <input type="date" name="datefinish">
            <br><br>
            <input type="submit" value="Send">
        </form>
        <?php
    }
    else
    {
        header("Location: index.php");
        exit();
    }
    $error = 0;

    if(isset($_GET['error']))
        $error = $_GET['error'];

    if($error==1)
        echo "Some fields are empty!";
    ?>
    </body>
    </html>
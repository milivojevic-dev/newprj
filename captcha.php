<?php
session_start();
unset($_SESSION["code"]);

/*
    48-57 0 - 9         ASCII tabela
    65-90 A-Z
    97-122 a-z

    */

$length = mt_rand(3,8);     // random broj od 3 do 8
$down = 65;
$up = 90;
$downMala = 97;
$upMala = 122;
$i = 0;
$password = "";

while($i<$length) {
    $bira = mt_rand(0,1);                           //za 0 stavlja mala slovo, a za 1 stavlja velika slovo

    if(($bira==1) && ($i%2==0)){                           //veliko slovo     i&&2==0 to gleda da li je parna pozicija i samo na parnu ce staviti veliko slovo
        $character = chr(mt_rand($down,$up));
        $password .= $character;
        $i++;
    }
    else{                                           //mala slova
        $character = chr(mt_rand($downMala,$upMala));
        $password .= $character;
        $i++;
    }
}

$_SESSION["code"] = $password;
$dan = Date('d');       // to je kao od 01 do 31 ali nema veze cita ga kao 1 do 31
if($dan % 2 == 0){
    $red = 0;
    $green = 0;
    $blue = 255;
}
else{
    $red = 255;
    $green = 0;
    $blue = 0;
}


header("Content-type: image/png");
$im   = imagecreatefrompng("captcha.png") or die("Cannot Initialize new GD image stream");
$text_color = imagecolorallocate($im, $red,$green,$blue);
imagettftext($im, 11, 0, 5, 20, $text_color, "arial.ttf", $password);
imagepng($im);
imagedestroy($im);
unset($password);



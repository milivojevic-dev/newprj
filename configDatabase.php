<?php


define("HOST","localhost");
define("USER","root");
define("PASSWORD","");
define("DATABASE","projects");

function connectToDb(){

    $connection = mysqli_connect(HOST, USER, PASSWORD, DATABASE) or die(mysqli_error($connection));

    mysqli_query($connection,"SET NAMES utf8");
    mysqli_query($connection,"SET CHARACTER SET utf8");
    mysqli_query($connection,"SET COLLATION_CONNECTION='utf8_general_ci'");
    return $connection;
}

$connection = connectToDb();

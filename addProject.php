
<?php
session_start();
include("configDatabase.php");
if(isset($_SESSION["id_user"])) {
    $category = mysqli_real_escape_string($connection, trim($_POST["category"]));
    $status = strip_tags(mysqli_real_escape_string($connection, trim($_POST["status"])));
    $datestart = mysqli_real_escape_string($connection, $_POST["datestart"]);
    $datefinish = mysqli_real_escape_string($connection, $_POST["datefinish"]);
    $price = mysqli_real_escape_string($connection, trim($_POST["price"]));



    if (empty($category) || empty($status) || empty($price) || empty($datestart) || empty($datefinish) || empty($_FILES["image"]["tmp_name"])) {
        header("Location: newProject.php");
        exit();
    }

    $check = getimagesize($_FILES["image"]["tmp_name"]);

    if ($check === false || $_FILES["image"]["type"] != "image/jpeg") {
        header("Location: index.php?error=1");
        exit();
    }

    $_FILES["image"]["name"] = time() . ".jpg";             // ime slike je aktuelni vremenski zig

    move_uploaded_file($_FILES["image"]["name"], "images/");        // destinacija je direktorijum images

    $sql = "INSERT INTO project VALUES(NULL, '$category', '" . $_FILES["image"]["name"] . "' , '$datestart', '$datefinish', '$price','$status')";
    mysqli_query($connection, $sql);


    exit();
}else {
    header("Location: index.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
</head>
<body>

<table>
    <?php
    $sql = "SELECT * FROM project p
JOIN category c on c.id_category=p.id_category";
    $result = mysqli_query($connection, $sql);
    if(mysqli_num_rows($result) > 0)
    {
        while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
        {
            echo "<tr><td>".$row["id_project"]."</td>";
            echo "<td>".$row["name"]."</td>";
            echo "<td><img src='images/".$row["image"]."' alt ='slika'></td>";
            echo "<td>".$row["datestart"]."</td>";
            echo "<td>".$row["datefinish"]."</td>";
            echo "<td>".$row["price"]."</td>";
            echo "<td>".$row["status"]."</td></tr>";
        }
    }


    ?>
    <tr><td><a href="newProject.php">Vrati se na New Project</a></td></tr>
</table>


</body>
</html>
